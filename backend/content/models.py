import datetime

from django.db import models
from django.utils import timezone

# Create your models here.

class Platform(models.Model):
    def __str__(self):
        return self.platform_name

    platform_name = models.CharField(max_length=200)

class Article(models.Model):
    def __str__(self):
        return self.article_title

    platform = models.ForeignKey(Platform, on_delete=models.CASCADE)
    article_title = models.CharField(max_length=200)
    hyperlink_url = models.TextField()
    favorite = models.BooleanField(default=False)
